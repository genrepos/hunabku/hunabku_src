#!/bin/sh

CWD=`pwd`
PROGNAME="setup_environment.sh"

clean_up()
{
   unset EULA LIST_MACHINES VALID_MACHINE
   unset CWD TEMPLATES SHORTOPTS LONGOPTS ARGS PROGNAME
   unset generated_config updated
   unset MACHINE SDKMACHINE DISTRO OEROOT
}

if [ "$(whoami)" = "root" ]; then
    echo "ERROR: do not use the BSP as root. Exiting..."
	return 1
fi

if [ -z "$MACHINE" ]; then
    echo "ERROR: No machine specified. Exiting..."
    return 1
fi

# Check the machine type specified
LIST_MACHINES=`ls -1 $CWD/*/conf/machine`
VALID_MACHINE=`echo -e "$LIST_MACHINES" | grep ${MACHINE}.conf$ | wc -l`
if [ "x$MACHINE" = "x" ] || [ "$VALID_MACHINE" = "0" ]; then
    echo -e "\nThe \$MACHINE you have specified ($MACHINE) is not supported by this build setup"
    clean_up
    return 1
else
    if [ ! -e $1/conf/local.conf.sample ]; then
        echo "Configuring for ${MACHINE}"
    fi
fi

# Todo: Ensure all files in /base are kept in sync

#
# Set/call OE tools
#
OEROOT=$CWD
echo "Build_dir = $1"
. $OEROOT/oe-init-build-env $CWD/$1 > /dev/null
# after sourcing "oe-init-build-env" you will be in build directory
# if conf/local.conf not generated, no need to go further
if [ ! -e conf/local.conf ]; then
    clean_up && return 1
fi
# Clean up PATH, because if it includes tokens to current directories somehow,
# wrong binaries can be used instead of the expected ones during task execution
export PATH="`echo $PATH | sed 's/\(:.\|:\)*:/:/g;s/^.\?://;s/:.\?$//'`"

generated_config=
if [ ! -e conf/local.conf.sample ]; then
	echo "Moving local.conf..."
    mv conf/local.conf conf/local.conf.sample

	# Set TEMPLATES dir
	# TEMPLATES are controlled by the layer meta-hunabku
    TEMPLATES=$CWD/sources/meta-hunabku/tools/templates 
    TEMPLATES_CONF=$TEMPLATES/conf

	#
    # Generate the local.conf based on Yocto defaults
	# and change settings according to json configuration
	# file.
	#
    grep -v '^#\|^$' conf/local.conf.sample > conf/local.conf
    cat >> conf/local.conf <<EOF

DL_DIR ?= "\${TOPDIR}/../../downloads"
#
# Parallelism Options
#
BB_NUMBER_THREADS ?= "7"
PARALLEL_MAKE ?= "-j 10"
EOF
    # Change settings 
    sed -e "s,MACHINE ??=.*,MACHINE ??= '$MACHINE',g" \
        -e "s,DISTRO ?=.*,DISTRO ?= '$DISTRO',g" \
        -e "s,PACKAGE_CLASSES ?=.*,PACKAGE_CLASSES ?= '$PKG_CLASSES',g" \
        -e "s,PACKAGECONFIG =.*,#,g" \
        -i conf/local.conf

	#
	# Copy the corresponding config templates
	# according to json config file.
	# Todo: handle bblayers.conf based on $HKU_MFG(manufacturer) key value
	# Tip: You can also use @site.conf file in Yocto for multiple build directories
	#
	cp $TEMPLATES_CONF/* conf/

    generated_config=1
fi

if [ -n "$generated_config" ]; then
    cat <<EOF
Your build environment has been configured with:

    MACHINE=$MACHINE
    DISTRO=$DISTRO
EOF
	clean_up
	return 0
else
    echo "Your configuration files at $1 have not been touched."
	clean_up
	return 1 
fi

