
SETUPSCRIPT="./setup_environment.sh"

HKU_PROGNAME="yocto_setup.sh"

# @hku_usage
# @Desc: Prints the use of this script 
hku_usage ()
{
   echo "
Usage: source $HKU_PROGNAME [build_dir [machine_name]] [options]

OPTIONS
       -b, --option-example option-example
              Explicitly set an option left here for future
			  reference.
       -h, --help
              Print out this help message.

If $HKU_PROGNAME is called with no build_dir or machine_name, 
it will exit.
"
}

hku_clean_up ()
{
   unset HKU_PROGNAME SCRIPT_SOURCE CONFFILE SETUPSCRIPT FSLSCRIPT
   unset SHORTOPTS LONGOPTS
   unset HKU_WD HKU_SDK_DIR
   unset HKU_ARGS HKU_BUILD_DIR HKU_MACHINE FSL_OPTS FSLDISTRO SETUP_OPTS
   unset hku_dry_run hku_setup_retval
   unset hku_usage hku_clean_up hku_check_base_sources
}

# Todo:
# Testing yocto_setup

# get cli options
SHORTOPTS="b:hm:n"
LONGOPTS="option-example:,option-example2:,help"
HKU_ARGS=$(getopt --options $SHORTOPTS  \
  --longoptions $LONGOPTS --name $HKU_PROGNAME -- "$@" )
# Print the usage menu if invalid options are specified
if [ $? != 0 ]; then
   hku_usage
   hku_clean_up
   return 1
fi

eval set -- "$HKU_ARGS"

while true;
do
   case $1 in
      -h|--help)
         set --
         hku_usage
         hku_clean_up
         return 0
         ;;

      -b|--option-example)
		 # Left here for reference only
		 # need to shift 2 because "-b 'option'"
         shift 2
         ;;

      --)
         shift
         break
         ;;
   esac
done

if [ -z "$1" ]
then
   echo "Error: empty option"
   hku_usage
   return 1
fi

HKU_BUILD_DIR=$1
HKU_MACHINE=$2
#
# Todo: 
# Check whether this script is updated or not

#
# final sanity check
#
echo -e "HKU_BUILD_DIR=$HKU_BUILD_DIR\nHKU_MACHINE=$HKU_MACHINE\n"

if [ ! -f $SETUPSCRIPT ]; then
   echo -e "\n$HKU_PROGNAME: file not found: $SETUPSCRIPT"
   hku_clean_up
   return 1
fi

#
# set MACHINE for the yocto build system
#
export MACHINE=$HKU_MACHINE

#
# Todo: How does $DISTRO will be handled ?
#
export DISTRO="hunabku-ti-bbb"

export PKG_CLASSES="package_ipk"

#
# Setup environment
#
eval . $SETUPSCRIPT $HKU_BUILD_DIR $HKU_MACHINE
hku_setup_retval=$?

if [ $hku_setup_retval -ne 0 ]; then
   echo -e "\n$SETUPSCRIPT failed!"
   return 1
fi
hku_clean_up
