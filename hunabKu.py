#!/usr/bin/python
# hunabKu
# This is the main script which will be charge to 
# parse the config file and run the neccessary scripts 
# in order to build an image. 
#
# Test: parse the json config file
import json
with open('config.json') as config_json_file:
    config_data = json.load(config_json_file)

print(type(config_data))
